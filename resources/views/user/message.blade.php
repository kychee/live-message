@extends('layouts.master')

@section('title', 'Live Messaging')

@section('content')
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Live Messaging</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('message.index')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Live Messaging</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    @include('layouts._partials.alert')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <a href="#createChatModal" data-modal="#createChatModal" data-toggle="modal" class="btn btn-info" style="margin-top: 20px">Create Chat</a>
        </div>
        <div class="m-portlet__body">
            <div class="m-section">
                <div class="m-section__content">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="m-portlet m-portlet--tabs">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Chat
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="m_portlet_tab_2_1">
                                            <div class="m-scrollable" data-scrollable="true" data-always-visible="1" data-rail-visible1="1" data-height="450" style="height: 500px; overflow: hidden;">
                                                <div id="messageContent" class="m-widget3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8" id="chatroom" style="display:none;">
                            <div class="m-portlet">
                                <div class="m-portlet__body" id="chat">
                                    <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                                        <div class="scrollDiv" style="height: 450px;overflow:hidden">
                                        <div class="m-messenger__messages m-scrollable" id="content" data-scrollable="true" data-always-visible="1" data-rail-visible1="1" data-height="450" style="height: 450px;" onscroll="showMore(this);"></div>
                                    </div>
                                        <div class="m-messenger__seperator"></div>
                                        <div class="m-messenger__form">
                                            <div class="m-messenger__form-controls">
                                                <textarea class="form-control m-messenger__form-input" style="resize:none;" id="reply" rows="1" placeholder="Type a message"></textarea>
                                            </div>
                                            <div class="m-messenger__form-tools">
                                                <span id="reply_trigger" class="m-messenger__form-reply d-none" title="Reply">
                                                    <i class="la la-send"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorLogModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn red btn-xs pull-right" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title"> Error Log </h4>
            </div>
            <div class="modal-body" id="errorMessageDIV">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createChatModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{route('message.newchat')}}" method="post">
            {!!csrf_field()!!}
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Create Chat</h4>
                </div>
                <div class="modal-body">
                    <h4>Enter Username:</h4>
                    <input type="text" name="username" class="form-control" placeholder="Username">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-outline">
                        Create
                    </button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="js-lightbox" class="cbp"></div>
{{-- <audio id="audiotag1" src="/music/notification.mp3" preload="auto"></audio> --}}
@stop

@section('page_style')
<link href="/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .cbp-popup-wrap{
        z-index:10000;
    }

    .m-widget3__item{
        cursor: pointer;
    }
    .m-messenger .m-messenger__messages .m-messenger__message {
        margin: 0 0 10px 0;
    }
    .m-messenger__message-content {
        padding: 10px 15px 5px 15px !important;
        white-space: pre-wrap;
        -moz-white-space: pre-wrap;
        word-wrap: break-word;
        word-break: break-all;
    }
    .m-messenger__message-text{
        font-size: 1.1rem !important;
    }
    .m-messenger__message-text > a{
        color: #fff179
    }
    .m-messenger__message-text > a:hover{
        color: #FFEB3B;
        text-decoration: underline;
        background-color: transparent;
    }
    .m-messenger__message-content .datetime {
        font-size: 0.8rem;
    }
    .m-messenger__message--in .m-messenger__message-content .datetime {
        color: #6f727d;
    }
    .m-messenger__message--out .m-messenger__message-content .datetime {
        color: white;
    }
    .m-messenger .m-messenger__form .m-messenger__form-tools .m-messenger__form-reply {
        border-radius: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -moz-justify-content: center;
        -ms-justify-content: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -moz-align-items: center;
        -ms-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        vertical-align: middle;
        height: 40px;
        width: 40px;
        text-align: center;
        vertical-align: middle;
        line-height: 0;
        cursor: pointer;
        background-color: #36a3f7;
        color: #fff;
    }
</style>
@stop

@section('page_script')
<script src="/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script>
    function formatChatList(result){
        var i = 0, html = "";
        if (result.length > 0){
            for(i; i < result.length; i++){
                if(result[i]){
                    if (result[i].type == 'user'){
                        html += '<div class="m-widget3__item" data-value="'+result[i].id+'">'+
                                    '<div class="m-widget3__header">'+
                                        '<div class="m-widget3__user-img">'+
                                            '<img class="m-widget3__img" src="/metronic_v5.1.1/metronic_v5.1.1/default/src/media/app/img/users/user2.jpg" alt="">'+
                                        '</div>'+
                                        '<div class="m-widget3__info">'+
                                            '<span class="m-widget3__username">'+
                                                result[i].username+
                                            '</span><br>'+
                                            '<span class="m-widget3__time">'+
                                                result[i].time+
                                            '</span>'+
                                        '</div>';
                        html += '</div>'+
                                    '<div class="m-widget3__body">'+
                                        '<p class="m-widget3__text">'+
                                            result[i].content_user +": "+ result[i].content+
                                        '</p>'+
                                    '</div>'+
                                '</div>';
                    }else{
                        html +=  '<div class="m-widget3__item" data-value="'+result[i].id+'">'+
                                    '<div class="m-widget3__header">'+
                                        '<div class="m-widget3__user-img">'+
                                            '<img class="m-widget3__img" src="/metronic_v5.1.1/metronic_v5.1.1/default/src/media/app/img/users/user2.jpg" alt="">'+
                                        '</div>'+
                                        '<div class="m-widget3__info">'+
                                            '<span class="m-widget3__username">'+
                                                result[i].username+
                                            '</span><br>'+
                                            '<span class="m-widget3__time">'+
                                                result[i].time+
                                            '</span>'+
                                        '</div>';
                        html += '</div>'+
                                    '<div class="m-widget3__body">'+
                                        '<p class="m-widget3__text">'+
                                            result[i].content+
                                        '</p>'+
                                    '</div>'+
                                '</div>';
                    }
                }
            }
        }else{
            html += '<div class="text-center"><p>No Record Available</p></div>';
        }

        return html;
    }

    function formatChatroom(){
        $('#chatroom').css('display', '');
    }

    function formatMessage(result){
        var i = 0, html = "";
		for(i; i < result.length; i++){
            if(result[i]){
                var message;
                message = result[i].content
               
                if (result[i].flow == "out"){
                    html += '<div class="m-messenger__wrapper">'+
                            '<div class="m-messenger__message m-messenger__message--out">'+
                                '<div class="m-messenger__message-body">'+
                                    '<div class="m-messenger__message-arrow"></div>'+
                                    '<div class="m-messenger__message-content">'+
                                        '<div class="m-messenger__message-username">'+
                                            result[i].username+
                                        '</div>'+
                                        '<div class="m-messenger__message-text">'+
                                            message+
                                        '</div>'+
                                        '<span class="datetime">'+result[i].time+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                }else{
                    html += '<div class="m-messenger__wrapper">'+
                            '<div class="m-messenger__message m-messenger__message--in">'+
                                '<div class="m-messenger__message-body">'+
                                    '<div class="m-messenger__message-arrow"></div>'+
                                    '<div class="m-messenger__message-content">'+
                                        '<div class="m-messenger__message-username">'+
                                            result[i].username+
                                        '</div>'+
                                        '<div class="m-messenger__message-text">'+
                                            message+
                                        '</div>'+
                                        '<span class="datetime">'+result[i].time+'</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                }
            }
        }
        return html;
    }

    function showMore(e){
        if ($('.m-messenger__messages')[0].scrollTop == 0){
            getMessage();
        }
    }

    function getMessage(){
        if (loading != 1){
            loading = 1;
            $('.m-messenger__messages').prepend('<div class="m-messenger__datetime"><div class="m-loader m-loader--info" style="width: 30px; display: inline-block;"></div></div>');
            $.get("/message/load?group_id="+current+"&skip="+skip, function(response){
                if(response.status == 1 && response.result.length > 0){
                    curScroll = $('.m-messenger__messages')[0].scrollHeight;
                    skip = response.skip;
                    $('.m-messenger__messages > div.m-messenger__datetime').remove();
                    $('.m-messenger__messages').prepend(formatMessage(response.result[0])).fadeIn('slow');
                    $('.m-messenger__messages').scrollTop($('.m-messenger__messages')[0].scrollHeight - curScroll);
                }else if (response.status == 2){
                    $('.m-messenger__messages > div.m-messenger__datetime').remove();
                }
                $('.m-messenger__form').css('display', '');
                loading = 0;
            });
        }
    }

    function enterChatroom(e){
        if (current != $(e).data('value') && loading != 1){
            current = $(e).data('value');
            var title = $(e).data('name');
            var userid = $(e).data('userid');

            skip = 0;
            $('.m-messenger__messages').empty();
            formatChatroom();
            getMessage();
            clearInterval(roomInterval);
            roomInterval = setInterval(function () {
                if (loading != 1){
                    loading = 1;
                    $.get("/message/loadnew?group_id="+current, function(response){
                        if(response.status == 1 && response.result.length > 0){
                            curScroll = 0;
                            $('.m-messenger__messages').append(formatMessage(response.result[0])).fadeIn('slow');
                            $('.m-messenger__messages').scrollTop($('.m-messenger__messages')[0].scrollHeight);
                        }
                        loading = 0;
                    });
                }
            }, 5000);
        }
    }

    function autoFill(e){
        var text = e.text;
        var reply = document.getElementById('reply');
        reply.value = reply.value + text + " ";
        Autosize.init();
    }

    function refreshChatroom(pagination){
        var query = '';
        if (pagination){
            query = query + "?" +pagination;
        }
        $('a[data-value="'+tab+'"]').prepend('<div class="m-loader m-loader--brand" style="width: 30px; display: inline-block;"></div>');
        $.get("/message/chatroom/" + query, function(response){
            if(response.status == 1 && response.result.length > 0){
                $('#messageContent').empty();
                $('#messageContent').append(formatChatList(response.result[0]));
                $('.t-pagination').remove();
                if (response.pagination){
                    $('#messageContent').parent().parent().append(response.pagination);
                }
                $('.nav.nav-tabs> li > a > div.m-loader.m-loader--brand').remove();
                notify(response.last_id, response.last_time);
            }
        });
    }

    function notify(id, time){
        if (lastId != id || lastTime != time){
            lastId = id;
            lastTime = time;
            // var promise = $('#audiotag1')[0].play();
            // if (promise) {
            //     //Older browsers may not return a promise, according to the MDN website
            //     promise.catch(function(error) { console.error(error); });
            // }
        }
    }
</script>
@stop

@section('init_script')
<script>
    var curScroll, listInterval, roomInterval, url, tab = "active";
    var current = skip = loading = lastId = lastTime = 0;
    var Autosize = {
        init: function() {
            var t;
            t = $("#reply"),
            autosize(t),
            autosize.update(t)
        }
    };

    $(function(){
        refreshChatroom(url);
        Autosize.init();
        
        setInterval(function(){
            refreshChatroom(url);
        }, 5000);


        {{-- initialize load chatroom --}}
        $('#activeTab').trigger('click');

        {{--  bind enter chatroom event  --}}
        $('#m_portlet_tab_2_1').on('click', '.m-widget3__item', function(){
            enterChatroom(this);
        });

        {{--  ajax pagination  --}}
        $('#m_portlet_tab_2_1').on('click', '.t-pagination>.pagination>li>a', function(e){
            e.preventDefault();
            url = $(this).attr('href').split('?')[1];
            refreshChatroom(url);
        });

        $('#reply').on('keydown', function(e){
            if ((e.which == 13 || e.key === 'Enter') && !e.shiftKey){
                e.preventDefault();
                var value = $.trim($('#reply').val());
                if (value.length > 0){
                    $(this).attr('disabled', 'disabled'); 
                    $.ajax({
                        method: "POST",
                        url: "/message/reply?group_id="+current,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            content:value
                        },
                        success: function (response) {
                            if (response.status == 1) {
                                $('.m-messenger__messages').append(formatMessage(response.result[0])).fadeIn('slow');
                            }
                        }, 
                        error: function (response) {
                            alert('Invalid Action');
                        },
                        complete: function (response) {
                            $('#reply').removeAttr('disabled');
                            //$('.scroller').scrollTop($('.chats')[0].scrollHeight);
                            $('.m-messenger__messages').scrollTop($('.m-messenger__messages')[0].scrollHeight);
                            $('#reply').focus();
                        }
                    });
                }
                $('#reply').val('');
                Autosize.init();
            }
        });

        $("#reply_trigger").on('click', function(){
            var e = $.Event('keydown');
            e.which = 13;
            $('#reply').trigger(e);
        });

        $('#js-lightbox').cubeportfolio({
            filters: '#js-filters-juicy-projects',
            loadMore: '#js-loadMore-juicy-projects',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'overlayBottomReveal',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '',

            // singlePage popup
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '',
            singlePageCallback: function(url, element) {
                var t = this;
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 10000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
            },
        });
    });  
</script>
@stop