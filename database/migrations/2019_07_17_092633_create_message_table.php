<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id');
            $table->integer('recipient_id');
            $table->integer('type');
            $table->string('content');
            $table->string('img_path')->nullable();
            $table->boolean('user_seen')->default(0);
            $table->boolean('status')->default(1);
            $table->integer('created_by');
            $table->timestamps();
        });


        Schema::table('message', function (Blueprint $table) {
            $table->index('sender_id');
            $table->index('recipient_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message');
    }
}
