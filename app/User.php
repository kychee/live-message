<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const
        CODE_STATUS_ACTIVE  = 1,
        CODE_STATUS_BAN  = 2
    ;

    public function isBan(){
        return $this->status == self::CODE_STATUS_BAN ? true : false;
    }

    public function isActive(){
        return $this->status == self::CODE_STATUS_ACTIVE ? true : false;
    }

    public function scopeActive($query){
        return $query->where($this->table.'.status', self::CODE_STATUS_ACTIVE);
    }
}
