<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class MessageGroup extends Model
{
	protected $table = 'message_group';

    protected $guarded = [];

    const 
        CODE_STATUS_INACTIVE = 0,
        CODE_STATUS_ACTIVE = 1
    ;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function scopeActive($query){
        return $query->where($this->table.'.status', self::CODE_STATUS_ACTIVE);
    }

    public static function getRecipient($group_id, $sender){
        $query = self::where('group_id',$group_id)
                    ->where('user_id','!=',$sender)
                    ->first();
        return $query->user_id;
    }
    
    public function scopeLatest($query){
        return $query->orderBy($this->table.'.created_at', 'desc');
    }

}
