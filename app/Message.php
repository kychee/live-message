<?php

namespace App;

use Log;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $table = 'message';

    protected $guarded = [];

    const 
        CODE_STATUS_INACTIVE = 0,
        CODE_STATUS_ACTIVE = 1,
     
        CODE_TYPE_TEXT = 1,
        CODE_TYPE_IMAGE = 2,

        CODE_SEEN_UNREAD = 0,
        CODE_SEEN_READ = 1
    ;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function lastMessage(){
        return Message::where('group_id', $this->group_id)->orderBy('created_at', 'desc')->active()->first();
    }

    public function getRecipientUsername(){
        $query = self::join('users','users.id','=','message.recipient_id')
                    ->where('message.recipient_id',$this->recipient_id)
                    ->first();
        return $query->username;
    }

    public function getSenderUsername(){
        $query = self::join('users','users.id','=','message.sender_id')
                    ->where('message.sender_id',$this->sender_id)
                    ->first();
        return $query->username;
    }

    public function scopeActive($query){
        return $query->where($this->table.'.status', self::CODE_STATUS_ACTIVE);
    }

    public function scopeLatest($query){
        return $query->orderBy($this->table.'.created_at', 'desc');
    }
}
