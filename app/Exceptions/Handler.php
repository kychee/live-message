<?php

namespace App\Exceptions;

use Exception;
use Log;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
            return redirect()->back()->with('msg-class', 'error')->with('msg', 'Invalid action')->withInput();
        }

        if ($e instanceof TokenMismatchException){
            return redirect()->back()->with('msg-class', 'alert-danger')->with('msg', 'Token mismatch, please try again')->withInput();
        }
        
        if ($this->isHttpException($e)) {
            switch ($e->getStatusCode()) {
                case '403':
                case '404':
                case '405':
                    return back()->with('msg-class','alert-danger')->with('msg',['Page Not Found']);
                break;
            }
        }

        return parent::render($request, $e);
    }
}
