<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;
use Log;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });

        Validator::extend('no_utf', function($attr, $value){
            return !(preg_match("/\p{Han}+/u", $value));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
