<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware'=>['guest','web']], function(){
	Route::get('/', ['as'=>'login', function(){ return view('user.login');}]);
	Route::group(['namespace'=>'User'], function(){
		Route::post('/', ['as'=>'login.post', 'uses' => 'UserController@login']);
		Route::post('/register', ['as'=>'register.post', 'uses' => 'UserController@registerStore']);
		Route::get('/logout', ['as'=>'logout', 'uses' => 'UserController@logout']);
	});
	// Route::get('/forgotpassword', ['as'=>'forgotpassword', function(){ return view('user.forgotpassword.index'); } ]);
	// Route::post('/forgotpassword', ['as'=>'forgotpassword.post', 'uses' => 'UserController@forgotpassword']);
});

Route::group(['middleware'=>['web','auth']], function(){
	Route::group(['namespace'=>'User'], function(){
		Route::group(['prefix'=>'message','as' => 'message.'], function(){
			Route::get('/', ['as'=>'index', 'uses' => 'MessageController@index']);
			Route::post('/newchat', ['as' => 'newchat', 'uses' => 'MessageController@newChat']);
			Route::get('/load', ['as' => 'load', 'uses' => 'MessageController@load']);
			Route::get('/loadnew', ['as' => 'loadnew', 'uses' => 'MessageController@loadnew']);
			Route::post('/reply', ['as' => 'reply', 'uses' => 'MessageController@reply']);
			Route::get('/chatroom', ['as' => 'chatroom', 'uses' => 'MessageController@chatroom']);
		});
	});
});
