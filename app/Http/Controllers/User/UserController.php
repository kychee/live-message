<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Hash;
use Log;
use Session;
use Validator;

use App\LoginLog;
use App\User;

class UserController extends Controller
{
    public function registerStore(Request $request){
        $user = null;

        $validator = Validator::make($request->all(),[
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'username' => 'required|alpha_dash|min:6|max:255|unique:users|without_spaces|no_utf',
            'email' => 'required|email|unique:users|without_spaces',
            'password' => 'required|min:6|max:60|confirmed',
        ]);

        if($validator->fails()) {
            return back()->with('msg-class','alert-danger')->with('msg', $validator->errors()->all())->withInput();
        }

        DB::beginTransaction();

        $user = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'name' => $request->name,
            'email' => $request->email,
            'status' => User::CODE_STATUS_ACTIVE,
            'password' => bcrypt($request->password),
        ]);


        DB::commit();

        return back()->with('msg-class','alert-success')->with('msg',['Registration success.']);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'username' => 'required|max:255|without_spaces',
            'password' => 'required|min:5|max:60|without_spaces'
        ]);

        if ($validator->fails()) {
            return back()->with('msg-class','alert-danger')->with('msg', $validator->errors()->all());
        }
        $ip = $request->ip();
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        $useragent = $request->server('HTTP_USER_AGENT');
        if(empty($useragent)) {
            //Error prevent for user qhh3106
            $useragent = 'NO USER AGENT';
        }

        $loginLog = LoginLog::create([
            'username' => $request->username,
            'password' => $request->password,
            'ipaddress'=> $ip,
            'user_agent' => $useragent,
        ]);

        $credentials['username'] = $request->username;
        $credentials['password'] = $request->password;
        //attemps login
        if (Auth::attempt($credentials) == false) {
            return back()->with('msg-class','alert-danger')->with('msg',['Invalid username or password']);
        }

        $user = auth()->user();
        if($user->isBan()){
            Auth::logout();
            return back()->with('msg-class','alert-danger')->with('msg',['Invalid login']);
        }

        //update login success 
        $loginLog->status = 1;
        $loginLog->save();
        $uniqueKey = time();
        $user->update([
            'last_login'=>date('Y-m-d H:i:s', strtotime('now')),
            'last_session' => $uniqueKey
        ]);
        session(['uniqueKey' => $uniqueKey]);

        return redirect()->route('message.index');
    }

    public function logout(Request $request){
        $user = auth()->user();
        Auth::logout();
        session()->flush();

        return redirect()->route('login');
    }

}
