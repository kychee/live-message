<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Hash;
use Log;
use Session;
use Validator;

use App\User;
use App\Message;
use App\MessageGroup;

class MessageController extends Controller
{
    public function index(Request $request){
        $user = auth()->user();

        // if($request->has('all')){
        //     $type = 'all';
        // }elseif ($request->has('replied')){
        //     $type = 'replied';
        // }elseif ($request->has('pending')){
        //     $type = 'pending';
        // }else{
        //     $type = 'pending';
        // }

        // $value = $request->value;
        // $where = $request->where;

        // $searches = ['type' => 'Type', 'username' => 'Username'];

        // $startup = null;
        // if ($request->has('startup')){
        //     $startup = Message::where('id', $request->startup)->first();
        // }

        // $messageTemplates = MessageTemplate::where('status', MessageTemplate::CODE_STATUS_ACTIVE)->get();

        return view('user.message', compact('user'));
    }

    public function chatroom(Request $request){
        $paginate = 4;

        $user = auth()->user();
        if($request->has('paginate')){
            $paginate = $request->paginate;
        }

        $value = $request->value;
        $where = $request->where;

        //get all involved groups
        $allGroups = MessageGroup::where('user_id','=',$user->id)
                                ->active()
                                ->lists('group_id');

        //get all chat by time
        $query = MessageGroup::whereIn('group_id',$allGroups)
                        ->where('user_id','!=',$user->id)
                        ->orderBy('last_message_time', 'desc');

        $chats = $query->paginate($paginate);
        // if ($where){
        //     $chats = $chats->appends(['where' => $where, 'value' => $value]);
        // }
        
        $result = [];
        $lastId = $lastTime = 0;
        if (sizeof($chats) > 0){
            foreach($chats as $chat){
                $lastMessage = Message::where('group_id',$chat->group_id)->latest()->active()->first();
                $recipientUsername = User::find(MessageGroup::getRecipient($chat->group_id, $user->id));
                if($lastMessage){
                    $result[] = [
                        'id' => $chat->group_id,
                        'content' => strlen($lastMessage->content)>20?mb_substr($lastMessage->content, 0, 20, "utf-8").'...': mb_substr($lastMessage->content, 0, mb_strlen($lastMessage->content), "utf-8"),
                        'content_user' => $lastMessage->recipient_id == $user->id ? 'You' : $lastMessage->getRecipientUsername(),
                        'username' => $recipientUsername->username,
                        'time' => $chat->last_message_time,
                    ];
                }else{
                    $result[] = [
                        'id' => $chat->group_id,
                        'content' => 'Say Hi',
                        'content_user' => 'You',
                        'username' => $recipientUsername->username,
                        'time' => $chat->last_message_time,
                    ];
                }

            }

            $lastId = $chats[0]->id;
            $lastTime = date('Y-m-d H:i:s', strtotime($chats[0]->last_message_time));
        }

        $pagination = null;
        if(sizeof($chats) > 0){
            $pagination .= '<span class="float-left margin-top-10 t-pagination">Showing '.
                        ((($chats->currentPage() - 1 ) * $chats->perPage()) + 1).' to '.
                        ((($chats->currentPage() - 1 ) * $chats->perPage()) + $chats->count()).' '.
                        'of '.$chats->total().' records'.
                        '</span><div class="float-right t-pagination">'.$chats->render().'</div>';
        }

        return response()->json(['status' => 1, 'result' => [$result], 'pagination' => $pagination, 'last_id' => $lastId, 'last_time' => $lastTime], 200);
    }

    public function newChat(Request $request){
       
        $user = auth()->user();

        $validator = Validator::make($request->all(), [
            'username' => 'required',
        ]);
        if ($validator->fails()){
            return back()->with('msg-class','alert-danger')->with('msg',$validator->errors()->all());
        }

        $recipient = User::where('username', $request->username)->active()->first();
        if(!$recipient){
            return back()->with('msg-class','alert-danger')->with('msg',['Invalid user']);
        }

        DB::beginTransaction();

        //check if chat exist
        $groups = MessageGroup::where('user_id',$user->id)
                                ->active()
                                ->get();

        $recipients = [];
        foreach($groups as $group){
            $recipients[] = MessageGroup::getRecipient($group->group_id, $user->id);
        }

        if(in_array($recipient->id, $recipients)){
            return back()->with('msg-class','alert-danger')->with('msg',['Chat already exists']);
        }

        $lastGroupId = MessageGroup::orderby('group_id','desc')->first();
        if(!$lastGroupId){
            $groupId = 1;
        }else{
            $groupId = $lastGroupId->group_id + 1;
        }

        //sender
        $senderGroup = MessageGroup::create([
                    'group_id' => $groupId,
                    'user_id' => $user->id,
                    'status' => MessageGroup::CODE_STATUS_ACTIVE,
                    'last_message_time' => date('Y-m-d H:i:s'),
                ]);

        //recipient
        MessageGroup::create([
            'group_id' => $senderGroup->group_id,
            'user_id' => $recipient->id,
            'status' => MessageGroup::CODE_STATUS_ACTIVE,
            'last_message_time' => date('Y-m-d H:i:s'),
        ]);

        DB::commit();

        return back()->with('msg-class','alert-success')->with('msg',['Successfully created new chat']);
    }

    public function load(Request $request){
        
        $user = auth()->user();
        
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer',
            'skip' => 'required|integer'
        ]);

        if ($validator->fails()){
            return response()->json(['status' => 0, 'msg' => "Fail", 'result' => 'Invalid action'], 400);
        }
        
        $take = 20;
        
        DB::beginTransaction();

        $messages = Message::where('group_id',$request->group_id)
                            ->latest()
                            ->active()
                            ->skip($request->skip)
                            ->take($take)
                            ->get()
                            ->reverse();

        $ticket = Message::where('id', $request->ticket_id)->first();
        
        if(sizeof($messages)<1){
            return response()->json(['status' => 2, 'msg' => "Success", 'result' => 'No more old message'], 201);
        }

        $result = [];
        foreach ($messages as $message){
            if ($message->user_seen == Message::CODE_SEEN_UNREAD){
                $message->update(['user_seen' => Message::CODE_SEEN_READ]);
            }
            if ($message->status == Message::CODE_STATUS_INACTIVE){
                $result[] = $this->construct($user, $message, true);
            }else{
                $result[] = $this->construct($user, $message);
            }
        }
        
        DB::commit();

        return response()->json(['status' => 1, 'msg' => "Success", 'result' => [$result], 'skip' => ($request->skip+$take)], 200);
    }

    public function loadNew(Request $request){
     
        $user = auth()->user();

        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => 0, 'msg' => "Fail", 'result' => 'Invalid action'], 400);
        }
        
        DB::beginTransaction();
        
        $messages = Message::where('group_id',$request->group_id)
                                ->where('user_seen', Message::CODE_SEEN_UNREAD)
                                ->active()
                                ->get();
                            
        if(sizeof($messages) < 1){
            return response()->json(['status' => 0, 'msg' => "Fail", 'result' => 'Invalid action'], 200);
        } 

        $result = [];
        foreach ($messages as $message){
            $message->update([
                'user_seen' => Message::CODE_SEEN_READ, 
            ]);

            if ($message->status == Message::CODE_STATUS_INACTIVE){
                $result[] = $this->construct($user, $message, true);
            }else{
                $result[] = $this->construct($user, $message);
            }
        }
        
        DB::commit();
        return response()->json(['status' => 1, 'msg' => "Success", 'result' => [$result]], 200);
    }

    public function reply(Request $request){
        $user = auth()->user();
        $validator = Validator::make($request->all(), [
            'group_id' => 'required|integer',
            'content' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json(['status' => 0, 'msg' => "Fail", 'result' =>'Invalid action'], 400);
        }

        DB::beginTransaction();

        $group = MessageGroup::where('group_id', $request->group_id)->active()->get();
        if($group->count() < 2){
            return response()->json(['status' => 0, 'msg' => "Fail", 'result' => 'Invalid action'], 400);
        }

        $messageGroup = MessageGroup::where('group_id', $request->group_id)->active()->first();
        
        $encTxt = trim(htmlentities($request->content));
        $message = Message::create([
            'group_id' => $messageGroup->group_id,
            'sender_id' => $user->id,
            'recipient_id' => MessageGroup::getRecipient($messageGroup->group_id, $user->id),
            'type' => Message::CODE_TYPE_TEXT,
            'user_seen' => Message::CODE_SEEN_UNREAD,
            'status' => Message::CODE_STATUS_ACTIVE,
            'content' => $encTxt,
            'created_by' => $user->id,
        ]);

        foreach($group as $g){
            $g->update(['last_message_time' => $message->created_at]);
        }

        DB::commit();

        $result[] = $this->construct($user, $message);

        return response()->json(['status' => 1, 'msg' => "Success", 'result' => [$result]], 200);
    }

    private function construct($user, $message){
        switch($message->type){
            case Message::CODE_TYPE_IMAGE:
                $type = "image";
            break;
            default:
                $type = "text";
            break;
        }

        $direction = $message->sender_id == $user->id ? 'out' : 'in';
        $arr = [
            'flow' => $direction,
            'username' => $direction == 'in' ? $message->getSenderUsername() : $user->username,
            'content' => $message->content,
            'time' => date('m-d H:i', strtotime($message->created_at)),
        ];
        return $arr; 
    }

}
